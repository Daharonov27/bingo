function BingoHeader(props){

    return (
        <div className="col border-end" style={{height: "30px", paddingTop: "4px", backgroundColor: "#fad390", opacity: "80%", color: "black"}}>
            <strong>
                {props.cellValue}
            </strong>
        </div>
    )
}

export default BingoHeader;
