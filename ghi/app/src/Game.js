import BingoSquare from "./BingoSquare";
import BingoHeader from "./BingoHeader";
import { useState } from "react";

function Game(){
    const [bingoCard, setBingoCard] = useState([["B2","I9","N6","G2","O8"], ["B1","I8","N1","G1","O9"], ["B4","I6","N8","G3","O4"], ["B7","I2","N4","G9","O6"], ["B9","I3","N5","G7","O7"]])


    return (
        <div className="container text-center">
            <h1 style={{marginBottom: "20px"}}>BINGO</h1>
            <div className="row align-items-center justify-content-end">
                <div className="col-6 border border-4">
                    <div className="row border-bottom border-4">
                        <BingoHeader cellValue={"B"} />
                        <BingoHeader cellValue={"I"} />
                        <BingoHeader cellValue={"N"} />
                        <BingoHeader cellValue={"G"} />
                        <BingoHeader cellValue={"O"} />
                    </div>
                    {bingoCard.map(row => {
                        return(
                            <div className="row border-bottom">
                                {row.map(col => {
                                    return(
                                        <BingoSquare cellValue={col}/>

                                    );
                                })}
                            </div>
                        )
                    })}
                </div>
                <div className="col-3">
                    <div className="row justify-content-center">
                        <div className="col-6">
                            <div className="border" style={{padding: "10px", borderRadius: "58px", width: "50px", height: "50px", margin: "auto" }}>
                                G5
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-6">
                            <button type="button" className="btn btn-light btn-outline-info btn-sm" style={{marginTop: "10px"}}>Next Ball</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Game;
