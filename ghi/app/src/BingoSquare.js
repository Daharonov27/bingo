function BingoSquare(props){
    return (
        <div className="col border-end text-center" style={{height: "50px", paddingTop: "13px", backgroundColor: "#f8c291", opacity: "75%", color: "black"}}>
            {props.cellValue}
        </div>
    )

};

export default BingoSquare;
